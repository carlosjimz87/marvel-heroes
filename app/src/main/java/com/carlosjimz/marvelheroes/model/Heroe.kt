package com.carlosjimz.marvelheroes.model

import java.io.Serializable

data class Hero(
    val name:String,
    val  realName:String,
    val  height:String,
    val power: String,
    val abilities: String,
    val groups: String,
    val  photo:String
):Serializable