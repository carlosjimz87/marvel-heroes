package com.carlosjimz.marvelheroes.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.carlosjimz.marvelheroes.api.OperationCallback
import com.carlosjimz.marvelheroes.model.Hero
import com.carlosjimz.marvelheroes.repository.HeroRepoSource

class HeroViewModel (private val repository: HeroRepoSource): ViewModel() {

    private val _heroes = MutableLiveData<List<Hero>>().apply { value = emptyList() }
    val heroes: LiveData<List<Hero>> = _heroes

    private val _isViewLoading= MutableLiveData<Boolean>()
    val isViewLoading: LiveData<Boolean> = _isViewLoading

    private val _onMessageError= MutableLiveData<Any>()
    val onMessageError: LiveData<Any> = _onMessageError

    private val _isEmptyList= MutableLiveData<Boolean>()
    val isEmptyList: LiveData<Boolean> = _isEmptyList

//    init {
//        loadHeroes()
//    }


    fun loadHeroes(){
        _isViewLoading.postValue(true)
        repository.retrieveHeroes(object: OperationCallback<Hero> {
            override fun onError(error: String?) {
                _isViewLoading.postValue(false)
                _onMessageError.postValue( error)
            }

            override fun onSuccess(data: List<Hero>?) {
                _isViewLoading.postValue(false)

                if(data!=null){
                    if(data.isEmpty()){
                        _isEmptyList.postValue(true)
                    }else{
                        _heroes.value= data
                    }
                }
            }
        })
    }

}