package com.carlosjimz.marvelheroes.di

import androidx.lifecycle.ViewModelProvider
import com.carlosjimz.marvelheroes.repository.HeroRepo
import com.carlosjimz.marvelheroes.repository.HeroRepoSource
import com.carlosjimz.marvelheroes.viewmodel.ViewModelFactory


object Injection {

    private val heroRepoSource:HeroRepoSource = HeroRepo()
    private val heroViewModelFactory = ViewModelFactory(heroRepoSource)

    fun provideRepository():HeroRepoSource{
        return heroRepoSource
    }

    fun provideViewModelFactory(): ViewModelProvider.Factory{
        return heroViewModelFactory
    }
}