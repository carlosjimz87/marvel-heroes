package com.carlosjimz.marvelheroes.repository

import android.util.Log
import com.carlosjimz.marvelheroes.api.ApiClient
import com.carlosjimz.marvelheroes.api.HeroResponse
import com.carlosjimz.marvelheroes.api.OperationCallback
import com.carlosjimz.marvelheroes.model.Hero
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


const val TAG= "HeroRepo"

class HeroRepo:HeroRepoSource {

    private var call: Call<HeroResponse>?=null

    override fun retrieveHeroes(callback: OperationCallback<Hero>) {
        call= ApiClient.build()?.heroes()
        call?.enqueue(object : Callback<HeroResponse> {
            override fun onFailure(call: Call<HeroResponse>, t: Throwable) {
                callback.onError(t.message)
            }

            override fun onResponse(call: Call<HeroResponse>, response: Response<HeroResponse>) {
                response.body()?.let {
                    if(response.isSuccessful && (it.isSuccess())){
                        Log.v(TAG, "data ${it.superheroes}")
                        callback.onSuccess(it.superheroes)
                    }else{
                        callback.onError("Error retrieving data!")
                    }
                }
            }
        })
    }

    override fun cancel() {
        call?.let {
            it.cancel()
        }
    }
}