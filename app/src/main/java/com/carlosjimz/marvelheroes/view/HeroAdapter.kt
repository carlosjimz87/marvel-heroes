package com.carlosjimz.marvelheroes.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.carlosjimz.marvelheroes.R
import com.carlosjimz.marvelheroes.model.Hero

class HeroAdapter(private var heroes:List<Hero>, var clickListener: OnItemListener): RecyclerView.Adapter<HeroAdapter.MViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_hero, parent, false)
        return MViewHolder(view)
    }

    override fun onBindViewHolder(vh: MViewHolder, position: Int) {
        vh.initialize(heroes[position],clickListener)
    }


    override fun getItemCount(): Int {
        return heroes.size
    }

    fun update(data:List<Hero>){
        heroes= data
        notifyDataSetChanged()
    }

    class MViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val textViewName: TextView = view.findViewById(R.id.textViewFullName)
        val imageView: ImageView = view.findViewById(R.id.imageView)
        val textViewDesc:TextView= view.findViewById(R.id.textViewFullDesc)

        fun initialize(item: Hero, action: OnItemListener){

            //render
            textViewName.text= item.name
            textViewDesc.text= Utils.ellipsize(item.abilities+" "+item.power,150)
            Glide.with(imageView.context).load(item.photo).into(imageView)

            itemView.setOnClickListener{
                action.onItemClick(item)
            }
        }
    }

    interface OnItemListener {
        fun onItemClick(hero:Hero)
    }
}