package com.carlosjimz.marvelheroes

import android.app.Application
import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.carlosjimz.marvelheroes.api.OperationCallback
import com.carlosjimz.marvelheroes.model.Hero
import com.carlosjimz.marvelheroes.repository.HeroRepoSource
import com.carlosjimz.marvelheroes.viewmodel.HeroViewModel
import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Captor
import org.junit.Before
import org.junit.Rule
import org.mockito.*

fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

@Suppress("UNCHECKED_CAST")
class ViewModelRequestHeroesTest {

    @Mock
    private lateinit var repository: HeroRepoSource
    @Mock
    private lateinit var context: Application

    @Captor
    private lateinit var operationCallbackCaptor: ArgumentCaptor<OperationCallback<Hero>>

    private lateinit var viewModel:HeroViewModel

    private lateinit var isViewLoadingObserver: Observer<Boolean>
    private lateinit var onMessageErrorObserver: Observer<Any>
    private lateinit var emptyListObserver: Observer<Boolean>
    private lateinit var onRenderHeroesObserver: Observer<List<Hero>>

    private lateinit var heroesEmptyList:List<Hero>
    private lateinit var heroesList:List<Hero>

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`<Context>(context.applicationContext).thenReturn(context)

        viewModel= HeroViewModel(repository)

        mockData()
        setupObservers()
    }

    @Test
    fun `retrieve heroes then returns empty data`(){
        with(viewModel){
            loadHeroes()
            isViewLoading.observeForever(isViewLoadingObserver)
            isEmptyList.observeForever(emptyListObserver)
            heroes.observeForever(onRenderHeroesObserver)
        }

        Mockito.verify(repository, Mockito.times(1)).retrieveHeroes(capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onSuccess(heroesEmptyList)

        assertNotNull(viewModel.isViewLoading.value)
        assertTrue(viewModel.isEmptyList.value==true)
        assertTrue(viewModel.heroes.value?.size==0)

    }

    @Test
    fun `retrieve heroes then returns full data`(){
        with(viewModel){
            loadHeroes()
            isViewLoading.observeForever(isViewLoadingObserver)
            heroes.observeForever(onRenderHeroesObserver)
        }

        Mockito.verify(repository, Mockito.times(1)).retrieveHeroes(capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onSuccess(heroesList)

        assertNotNull(viewModel.isViewLoading.value)
        assertTrue(viewModel.heroes.value?.size==3)
    }

    @Test
    fun `retrieve heroes then returns an error`(){
        with(viewModel){
            loadHeroes()
            isViewLoading.observeForever(isViewLoadingObserver)
            onMessageError.observeForever(onMessageErrorObserver)
        }
        Mockito.verify(repository, Mockito.times(1)).retrieveHeroes(capture(operationCallbackCaptor))
        operationCallbackCaptor.value.onError("An error occurred")
        assertNotNull(viewModel.isViewLoading.value)
        assertNotNull(viewModel.onMessageError.value)
    }

    private fun setupObservers(){
        isViewLoadingObserver= Mockito.mock(Observer::class.java) as Observer<Boolean>
        onMessageErrorObserver= Mockito.mock(Observer::class.java) as Observer<Any>
        emptyListObserver= Mockito.mock(Observer::class.java) as Observer<Boolean>
        onRenderHeroesObserver= Mockito.mock(Observer::class.java) as Observer<List<Hero>>
    }

    private fun mockData(){
        heroesEmptyList= emptyList()

        val mockList: MutableList<Hero> = mutableListOf()
        mockList.add(Hero("Spiderman", "Peter Benjamin Parker", "1.77m",power = "ppp",abilities = "aaa", groups = "ggg",photo = "photo"))
        mockList.add(Hero("Captain Marvel", "Carol Danvers", "1.80m",power = "ppp",abilities = "aaa", groups = "ggg",photo = "photo"))
        mockList.add(Hero("Hulk", "Robert Bruce Banner", "1.75m",power = "ppp",abilities = "aaa", groups = "ggg",photo = "photo"))

        heroesList =   mockList.toList()
    }
}