package com.carlosjimz.marvelheroes.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.carlosjimz.marvelheroes.R
import com.carlosjimz.marvelheroes.di.Injection
import com.carlosjimz.marvelheroes.model.Hero
import com.carlosjimz.marvelheroes.view.Utils.Companion.KEY
import com.carlosjimz.marvelheroes.viewmodel.HeroViewModel
import kotlinx.android.synthetic.main.activity_list_heroes.*
import kotlinx.android.synthetic.main.layout_error.*


class HeroesActivity : AppCompatActivity(), HeroAdapter.OnItemListener {

    private lateinit var viewModel: HeroViewModel
    private lateinit var adapter: HeroAdapter

    companion object {
        const val TAG= "LIST"
    }

    /**
    //Consider this, if you need to call the service once when activity was created.
    Log.v(TAG,"savedInstanceState $savedInstanceState")
    if(savedInstanceState==null){
    viewModel.loadHeroes()
    }
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_heroes)

        setupViewModel()
        setupUI()
    }

    //ui
    private fun setupUI(){
        adapter= HeroAdapter(viewModel.heroes.value?: emptyList(),this)
        recyclerView.layoutManager= LinearLayoutManager(this)
        recyclerView.adapter= adapter
    }

    private fun setupViewModel(){
        viewModel = ViewModelProvider(this, Injection.provideViewModelFactory()).get(HeroViewModel::class.java)
        //viewModel = ViewModelProvider(this,ViewModelFactory(Injection.providerRepository())).get(HeroViewModel::class.java)
        //viewModel = ViewModelProviders.of(this,ViewModelFactory(Injection.providerRepository())).get(HeroViewModel::class.java)
        viewModel.heroes.observe(this,renderHeroes)

        viewModel.isViewLoading.observe(this,isViewLoadingObserver)
        viewModel.onMessageError.observe(this,onMessageErrorObserver)
        viewModel.isEmptyList.observe(this,emptyListObserver)
    }

    //observers
    private val renderHeroes= Observer<List<Hero>> {
        Log.v(TAG, "data updated $it")
        layoutError.visibility= View.GONE
        layoutEmpty.visibility=View.GONE
        adapter.update(it)
    }

    private val isViewLoadingObserver= Observer<Boolean> {
        Log.v(TAG, "isViewLoading $it")
        val visibility=if(it)View.VISIBLE else View.GONE
        progressBar.visibility= visibility
    }

    private val onMessageErrorObserver= Observer<Any> {
        Log.v(TAG, "onMessageError $it")
        layoutError.visibility=View.VISIBLE
        layoutEmpty.visibility=View.GONE
        textViewError.text= "Error $it"
    }

    private val emptyListObserver= Observer<Boolean> {
        Log.v(TAG, "emptyListObserver $it")
        layoutEmpty.visibility=View.VISIBLE
        layoutError.visibility=View.GONE
    }

    //If you require updated data, you can call the method "loadMuseum" here
    override fun onResume() {
        super.onResume()
        viewModel.loadHeroes()
    }

    override fun onItemClick(hero: Hero) {
        val intent = Intent(this, FullActivity::class.java)
        intent.putExtra(KEY,hero)
        startActivity(intent)
    }

}
