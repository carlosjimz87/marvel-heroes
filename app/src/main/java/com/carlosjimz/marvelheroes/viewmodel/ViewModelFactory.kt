package com.carlosjimz.marvelheroes.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.carlosjimz.marvelheroes.repository.HeroRepoSource

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val repository:HeroRepoSource):ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HeroViewModel(repository) as T
    }
}