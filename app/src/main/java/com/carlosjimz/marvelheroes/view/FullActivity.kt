package com.carlosjimz.marvelheroes.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.carlosjimz.marvelheroes.R
import com.carlosjimz.marvelheroes.model.Hero
import kotlinx.android.synthetic.main.activity_full_hero.*


class FullActivity : AppCompatActivity() {


    companion object {
        const val TAG= "FULL"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_hero);

        val obj =  intent.getSerializableExtra(Utils.KEY) as Hero

        supportActionBar?.title = obj.name
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#EC2004")))

        textViewFullName.text = obj.name

        Glide.with(imageFullPhoto.context).load(obj.photo).into(imageFullPhoto)

        textViewFullRealname.text = Html.fromHtml(getString(R.string.hero_realname, obj.realName))
        textViewFullHeight.text = Html.fromHtml(getString(R.string.hero_height, obj.height))
        textViewFullFrom.text = Html.fromHtml(getString(R.string.hero_from, obj.groups))
        textViewFullDesc.text = Html.fromHtml(getString(R.string.hero_about, obj.power + " " + obj.abilities))

    }


}
