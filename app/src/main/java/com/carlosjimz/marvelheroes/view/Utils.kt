package com.carlosjimz.marvelheroes.view

class Utils {

    companion object {
        fun ellipsize(text: String, len: Int): String {
            return if (text.length >= len) text.substring(0, len) + "..." else text
        }


        const val KEY= "HERO"

    }
}