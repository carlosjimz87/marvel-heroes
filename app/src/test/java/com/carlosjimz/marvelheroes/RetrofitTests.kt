package com.carlosjimz.marvelheroes

import android.app.Application
import android.content.Context
import android.util.Log
import com.carlosjimz.marvelheroes.api.ApiClient
import com.carlosjimz.marvelheroes.api.HeroResponse
import com.carlosjimz.marvelheroes.api.OperationCallback
import com.carlosjimz.marvelheroes.model.Hero
import com.carlosjimz.marvelheroes.repository.TAG
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.*
import org.robolectric.RobolectricGradleTestRunner
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RetrofitCallTest {

    private var call: Call<HeroResponse>?=null


    @Test
    @Throws(Exception::class)
    fun `test response status`() {

        call= ApiClient.build()?.heroes()
        val response = call?.execute();
        assertEquals(200,response?.code())
    }

    @Test
    @Throws(Exception::class)
    fun `test response data size`() {

        call= ApiClient.build()?.heroes()
        val response = call?.execute();
        assertEquals(6,response?.body()?.superheroes?.size)
        println("Response: ${response?.body()}")
    }
}