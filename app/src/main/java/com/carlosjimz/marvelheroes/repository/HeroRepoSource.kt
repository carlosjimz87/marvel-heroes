package com.carlosjimz.marvelheroes.repository

import com.carlosjimz.marvelheroes.api.OperationCallback
import com.carlosjimz.marvelheroes.model.Hero

interface HeroRepoSource {
    fun retrieveHeroes(callback: OperationCallback<Hero>)
    fun cancel()
}
